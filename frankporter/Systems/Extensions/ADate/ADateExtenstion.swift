//
//  ADateExtenstion.swift
//  freightsoftware
//
//  Created by DK on 17/04/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    func generateDatesArrayBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date) ->[Date]
    {
        var datesArray: [Date] =  [Date]()
        var startDate = startDate
        let calendar = Calendar.current
        
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        
        while startDate <= endDate {
            datesArray.append(startDate)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return datesArray
    }
    
    func DatesAvailableBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date,SelectedDate:Date) ->Bool
    {
        var startDate = startDate
        let calendar = Calendar.current
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        while startDate <= endDate
        {
            if(startDate == SelectedDate)
            {
                return true
            }
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return false
    }    
    
}

