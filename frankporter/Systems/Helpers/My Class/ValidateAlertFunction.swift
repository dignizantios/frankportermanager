//
//  ValidateAlertFunction.swift
//  PizzyBites
//
//  Created by TecoCraft Troop on 03/06/17.
//  Copyright © 2017 TecoCraft Troop. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

//#MARK: - Validation

func isValidEmail(testStr:String) -> Bool {
    // println("validate calendar: \(testStr)")
    let emailRegEx = "^([A-Za-z0-9._%+-\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


func isValidPostCode(testStr:String) -> Bool {
    // println("validate calendar: \(testStr)")
    let postcodeRegEx = "^(GIR 0AA)|((([A-Z][0-9]{1,2})|(([A-Z][A-HJ-Y][0-9]{1,2})|(([A-Z][0-9][A-Z])|([A-Z][A-HJ-Y][0-9]?[A-Z])))) [0-9][A-Z]{2})$"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", postcodeRegEx)
    return emailTest.evaluate(with: testStr)
}
