//
//  DashBoardFutureTaskVC.swift
//  frankporter
//
//  Created by DK on 07/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

class DashBoardFutureTaskVC: UIViewController,IndicatorInfoProvider {

    //MARK:- Outlets
    
    @IBOutlet weak var tblTaskList: UITableView!
    
    //MARK:- Declaration
    var taskList : [JSON]?
   
    var taskOffset = 0
    let taskRefresh = UIRefreshControl()
    var strMsg = String()
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decore()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Future Task")
    }
    
    //MARK:- Custom Methods
    
    func decore()
    {
        self.taskList = []
        
        taskRefresh.addTarget(self, action: #selector(self.upperRefreshTable), for: .valueChanged)
        tblTaskList.addSubview(taskRefresh)
        
        self.edgesForExtendedLayout = .init(rawValue: 0)
        self.extendedLayoutIncludesOpaqueBars = true
      
        tblTaskList.dataSource = self
        tblTaskList.delegate = self
        tblTaskList.contentInset = UIEdgeInsetsMake(0, 0, 90, 0)
        
        getTaskList()
      
    }
}

//MARK: - RefreshController

extension DashBoardFutureTaskVC
{
    @objc func upperRefreshTable()
    {
        self.view.endEditing(true)
        taskOffset = 0
        getTaskList()
    }
}

//MARK:- Tablview Delegate
extension DashBoardFutureTaskVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if taskList == nil{
            return 10
        }
        if taskList?.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFontWithReduseSize(size: 18, fontname: .light)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return taskList!.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let TaskListCell = self.tblTaskList.dequeueReusableCell(withIdentifier: "TaskListCell") as! TaskListCell
        if taskList != nil
        {
            TaskListCell.stopAnimationSkeleton()
            TaskListCell.setData(data: taskList![indexPath.row])
        }
       
        return TaskListCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if taskList == nil{
            return
        }
        
        let TaskDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailsVC") as! TaskDetailsVC
        let task = taskList![indexPath.row]
        TaskDetailsVC.userType = task["role"].stringValue
        TaskDetailsVC.taskType = task["propertyType"].stringValue
        TaskDetailsVC.PropertyTitle = task["appartment"].stringValue
        TaskDetailsVC.taskDone = self
        TaskDetailsVC.dictData = task
        TaskDetailsVC.selectParent = .future
        self.navigationController?.pushViewController(TaskDetailsVC, animated: true)
    }
}

//MARK:- Delegate

extension DashBoardFutureTaskVC:delegateTaskDoneEdit
{
    func doneTaskCompleted() {
        upperRefreshTable()
    }
    
}

//MARK:- Service

extension DashBoardFutureTaskVC : NVActivityIndicatorViewable
{
    func getTaskList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if taskOffset >= 0
            {
                let url = "\(basicURL)\(strFutureTask)"
                
                let param:[String:String] = [:]
                
                print("URL: \(url)")
                
                print("Param : \(param)")
                
                startAnimating(Loadersize, message: strLoader, type: NVActivityIndicatorType(rawValue:LoaderType))
                
                CommonService().Service(url: url, param: param, header: getUserDetail("access_token"),isLogin: false) { (respones) in
                    
                    self.taskRefresh.endRefreshing()
                    self.stopAnimating()
                    
                    if let json = respones.value
                    {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == strFlagTrue
                        {
                            if self.taskOffset == 0
                            {
                                self.taskList = []
                            }
                            self.taskOffset = json["next_offset"].intValue
                            var aryData = json["data"].arrayValue
                            aryData = self.taskList! + aryData
                            self.taskList = aryData
                        }
                        else if json["flag"].stringValue == strFlagAccessDenid
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
                        }
                        else
                        {
                            self.taskList = []
                            self.taskOffset = json["next_offset"].intValue
                            self.strMsg = json["msg"].stringValue
                        }
                        self.tblTaskList.reloadData()
                    }
                    else
                    {
                        makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                    }
                }
            }
            else
            {
                taskRefresh.endRefreshing()
            }
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}


