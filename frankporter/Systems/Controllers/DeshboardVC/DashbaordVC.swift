//
//  DashbaordVC.swift
//  frankporter
//
//  Created by DK on 06/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Foundation
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

class DashbaordVC: ButtonBarPagerTabStripViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var TopBarView: ButtonBarView!
    
    //MARK:- Declaration
    
    //var currentIndex = 0
    
    
    //MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Frank porter"
        let RightNavigationButton =  UIButton()
        RightNavigationButton.setImage(UIImage(named: "ic_report_flag_header"), for: .normal)
        RightNavigationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        RightNavigationButton.addTarget(self, action: #selector(ReportAction(sender:)), for: .touchUpInside)
        let customBarItem = UIBarButtonItem(customView: RightNavigationButton)
        
        let btnLogout =  UIButton()
        btnLogout.setImage(UIImage(named: "ic_logout_header"), for: .normal)
        btnLogout.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnLogout.addTarget(self, action: #selector(btnLogoutAction(sender:)), for: .touchUpInside)
        let logOutItem = UIBarButtonItem(customView: btnLogout)
        
        self.navigationItem.rightBarButtonItems = [logOutItem,customBarItem]
        
        self.navigationItem.hidesBackButton = true
        
       // PagerTabStripBehaviour.progressive(skipIntermediateViewControllers: true, elasticIndicatorLimit: true)
        settings.style.buttonBarBackgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1.0)
        settings.style.buttonBarItemBackgroundColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1.0)
        settings.style.selectedBarBackgroundColor = UIColor.appThemeBtnColor
        settings.style.buttonBarItemFont = themeFontWithReduseSize(size: 16, fontname: themeFonts.light)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 1
        settings.style.buttonBarItemTitleColor = UIColor.appThemeBtnColor
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        // in case the barView items do not fill the screen width this property stretch the cells to fill the screen
        //settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        self.TopBarView.collectionViewLayout = UICollectionViewFlowLayout()
        self.TopBarView.frame.size.height = 40
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.appThemeLightFontColor
            newCell?.label.textColor = UIColor.appThemeBtnColor
            
        }
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = .init(rawValue: 0)
        self.extendedLayoutIncludesOpaqueBars = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    @objc func ReportAction(sender:UIButton)
    {
        let ReportIssueVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportIssueVC") as! ReportIssueVC
        self.navigationController?.pushViewController(ReportIssueVC, animated: true)
    }
    
    
    @objc func btnLogoutAction(sender:UIButton)
    {
        let alertController = UIAlertController(title: getCommonString(key: "FrankPorter_key"), message: getCommonString(key: "Do_you_want_to_logout_from_the_app_key"), preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
          
            
            self.userLogout()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    // MARK: - PagerTabStripDelegate
    
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
    }
    
   
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let DashboardTodatTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "DashboardTodatTaskVC") as! DashboardTodatTaskVC
         let DashBoardFutureTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardFutureTaskVC") as! DashBoardFutureTaskVC
        return [DashboardTodatTaskVC, DashBoardFutureTaskVC]
    }
    
    @IBAction func btnGoodToknowVC(_ sender: Any) {
        let GoodToKnowVC = self.storyboard?.instantiateViewController(withIdentifier: "GoodToKnowVC") as! GoodToKnowVC
        self.navigationController?.pushViewController(GoodToKnowVC, animated: true)
    }
    
    /*
     
     // MARK: Private
    private func progressiveIndicatorData(_ virtualPage: Int) -> (Int, Int, CGFloat) {
        let count = viewControllers.count
        var fromIndex = currentIndex
        var toIndex = currentIndex
        let direction = swipeDirection
        
        if direction == .left {
            if virtualPage > count - 1 {
                fromIndex = count - 1
                toIndex = count
            } else {
                if self.scrollPercentage >= 0.5 {
                    fromIndex = max(toIndex - 1, 0)
                } else {
                    toIndex = fromIndex + 1
                }
            }
        } else if direction == .right {
            if virtualPage < 0 {
                fromIndex = 0
                toIndex = -1
            } else {
                if self.scrollPercentage > 0.5 {
                    fromIndex = min(toIndex + 1, count - 1)
                } else {
                    toIndex = fromIndex - 1
                }
            }
        }
        let scrollPercentage = pagerBehaviour.isElasticIndicatorLimit ? self.scrollPercentage : ((toIndex < 0 || toIndex >= count) ? 0.0 : self.scrollPercentage)
        return (fromIndex, toIndex, scrollPercentage)
    }
    
    
    
    
     override func virtualPageFor(contentOffset: CGFloat) -> Int {
        return Int((contentOffset + 1.5 * pageWidth) / pageWidth) - 1
     }
     
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int){
        let virtualPage = virtualPageFor(contentOffset: containerView.contentOffset.x)
        let newCurrentIndex = pageFor(virtualPage: virtualPage)
        //currentIndex = newCurrentIndex
       // preCurrentIndex = currentIndex
        let oldCurrentIndex = currentIndex
        let changeCurrentIndex = newCurrentIndex != oldCurrentIndex
        
        if let progressiveDelegate = self as? PagerTabStripIsProgressiveDelegate, pagerBehaviour.isProgressiveIndicator {
            
            let (fromIndex, toIndex, scrollPercentage) = progressiveIndicatorData(virtualPage)
            progressiveDelegate.updateIndicator(for: self, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: scrollPercentage, indexWasChanged: changeCurrentIndex)
        } else {
          //  delegate?.updateIndicator(for: self, fromIndex: min(oldCurrentIndex, pagerViewControllers.count - 1), toIndex: newCurrentIndex)
            print("Not update the index")
        }
    }
   
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool){
       // TopBarView.move(fromIndex: fromIndex, toIndex: toIndex, progressPercentage: progressPercentage, pagerScroll: .no)
    }
    
    
    open override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        
        //barView.move(fromIndex: fromIndex, toIndex: toIndex, progressPercentage: progressPercentage)
    }
    
    open func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int) {
       // barView.moveTo(index: toIndex, animated: true)
    }*/

}


//MARK:- Other methods
extension DashbaordVC:NVActivityIndicatorViewable
{
    @objc func userLogout()
    {
        var url = String()
        
        url = "\(basicURL)\(strLogout)"
        
        print("URL:- \(url)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param:[String:String] = [:]
            
            
            
            print("parm \(param)")
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            //            show
            
            CommonService().Service(url: url, param: param,header: getUserDetail("access_token"),isLogin: false) { (respones) in
                //                dismiss
                self.stopAnimating()
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        Defaults.removeObject(forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                        for aViewController in viewControllers {
                            if aViewController is ViewController {
                                self.navigationController!.popToViewController(aViewController, animated: false)
                            }
                        }
                        
                    }
                    
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

