//
//  TaskDetailsVC.swift
//  frankporter
//
//  Created by DK on 07/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

struct Task {
    var tempTask: JSON?
    var taskImg: UIImage?
    var isTaskStarted:Bool?
    
    
    init(task: JSON, img: UIImage,started:Bool) {
        tempTask = task
        taskImg = img
        isTaskStarted = started
    }
}

protocol delegateTaskDoneEdit {
    func doneTaskCompleted()
}


class TaskDetailsVC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var tblTaskDetails: UITableView!
    
    //MARK:- Declaration
    var taskList:[Task] = []
    var userType = String()
    var taskType = String()
    var selectedIndex = Int()
    var PropertyTitle  = String()
    var dictData = JSON()
    var taskDone:delegateTaskDoneEdit?
    
    //MARK:- View lifecycle
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        decore()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Custom Methods
    
    func decore(){
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
         self.edgesForExtendedLayout = .init(rawValue: 0)
         self.extendedLayoutIncludesOpaqueBars = true
        
        tblTaskDetails.delegate = self
        tblTaskDetails.dataSource = self
        self.navigationController?.isNavigationBarHidden = false
        self.title = PropertyTitle
        let RightNavigationButton =  UIButton()
        RightNavigationButton.setImage(UIImage(named: "ic_arrow_back_header"), for: .normal)
        RightNavigationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        RightNavigationButton.addTarget(self, action: #selector(backAction(sender:)), for: .touchUpInside)
        let customBarItem = UIBarButtonItem(customView: RightNavigationButton)
        self.navigationItem.leftBarButtonItem = customBarItem
        self.navigationItem.hidesBackButton = true
        
        let arrTask:[JSON] = dictData["check_id"].arrayValue
       
        var task = JSON()
        
        if dictData["role"].stringValue == UserLogInType
        {
            task["paid_status"] = dictData["paid_status"]
            let tsk1 = Task(task: task, img: UIImage(), started: false)
            taskList.append(tsk1)
        }        
        
            if dictData["task_status"].stringValue.lowercased() == "todo"
            {
                task["title"] = "Start"
                task["picture_required"] = "false"
                //task["status"] = "false"
                task["isImgUploaded"] = "false"
                task["isTaskCompleted"] = "false"
                let tsk1 = Task(task: task, img: UIImage(), started: false)
                taskList.append(tsk1)
            }
            else
            {
                var task = JSON()
                task["title"] = "Start"
                task["picture_required"] = "false"
               // task["status"] = "false"
                task["isImgUploaded"] = "false"
                task["isTaskCompleted"] = "true"
                let tsk1 = Task(task: task, img: UIImage(), started: true)
                taskList.append(tsk1)
            }
        
        
            for i in 0..<arrTask.count
            {
                var dict = JSON()
                dict = arrTask[i]
                dict["isImgUploaded"] = "false"
                //dict["status"] = "false"
                if  dict["status"].stringValue == "0" {
                    dict["isTaskCompleted"] = "false"
                }
                else
                {
                   dict["isTaskCompleted"] = "true"
                }
                var isStarted = Bool()
                if dictData["task_status"].stringValue.lowercased() == "todo"
                {
                    isStarted = false
                }
                else{
                    isStarted = true
                }
                let task1 = Task(task: dict, img: UIImage(), started: isStarted)
                taskList.append(task1)
            }
            
            if dictData["task_status"].stringValue.lowercased() == "done"
            {
                task = JSON()
                task["title"] = "End"
                task["picture_required"] = "false"
                //task["status"] = "false"
                task["isImgUploaded"] = "false"
                task["isTaskCompleted"] = "true"
                let tsk7 = Task(task: task, img: UIImage(), started: true)
                taskList.append(tsk7)
            }
            else{
                task = JSON()
                task["title"] = "End"
                task["picture_required"] = "false"
             //   task["status"] = "false"
                task["isImgUploaded"] = "false"
                task["isTaskCompleted"] = "false"
                let tsk7 = Task(task: task, img: UIImage(), started: false)
                taskList.append(tsk7)
            }

       
        
        self.tblTaskDetails.reloadData()
        
    }
  
    @objc func backAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
   /* fileprivate func extractedFunc(_ sender: UIButton) {
        alertForConformation(tag:sender.tag, status: <#String#>)
    }*/
    
    @objc func showAlertWithConformation(sender: UIButton) {
        let task = taskList[sender.tag]
        let taskImgRequire = task.tempTask!["picture_required"].stringValue
        let isTaskStarted = task.isTaskStarted!
        if !isTaskStarted
        {
            if task.tempTask!["role"].stringValue == UserLogInType && sender.tag  == 1
            {
                if taskImgRequire == "true"
                {
                    showOpenCamera(sender: sender)
                }else{
                    alertForConformation(tag:sender.tag, status: "")
                }
            }
            else if sender.tag  == 0
            {
                if taskImgRequire == "true"
                {
                    showOpenCamera(sender: sender)
                }else{
                   // extractedFunc(sender)
                    alertForConformation(tag:sender.tag, status: "working")
                }
            }
            else{
                makeToastSnackbar(message: getValidationString(key: "Please_start_task_first_key"))
            }
        }else{
            if sender.tag != self.taskList.count
            {
                var isAllTaskCompleted = false
                var remaingTaskTitle = String()
                for (index) in 0...self.taskList.count
                {
                    let tskStatus = self.taskList[index].tempTask!["isTaskCompleted"].boolValue
                    if tskStatus
                    {
                        isAllTaskCompleted = true
                    }else
                    {
                        isAllTaskCompleted = false
                        remaingTaskTitle = self.taskList[index].tempTask!["title"].stringValue
                       
                    }
                }
                if isAllTaskCompleted{
                    if taskImgRequire == "true"
                    {
                        showOpenCamera(sender: sender)
                    }else{
                        alertForConformation(tag:sender.tag, status: "")
                    }
                }else{
                    let str1 = getValidationString(key: "Please_check_key")
                    let str2 = getValidationString(key: "task_is_reaming_key")
                    makeToastSnackbar(message: "\(str1) \(remaingTaskTitle) \(str2)")
                }
            }else{
                if taskImgRequire == "true"
                {
                    showOpenCamera(sender: sender)
                }else{
                    alertForConformation(tag:sender.tag, status: "done")
                }
            }
        }
    }
    func alertForConformation(tag:Int,status:String)
    {
        var strTitle = String()
        if status.lowercased() == "done"
        {
            strTitle = getCommonString(key: "Are_you_sure_this_task_is_completed_key")
        }
        else
        {
            strTitle = getCommonString(key: "Do_you_want_to_start_task_key")
        }
        
        let alert = UIAlertController(title: getCommonString(key: "FrankPorter_key"), message: strTitle, preferredStyle: .alert)
        let noAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertActionStyle.cancel) {
            (result : UIAlertAction) -> Void in
        }
        let yesAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            
            
            var task = self.taskList[tag]
            if status == "working" || status == "done"
            {
                   self.startTask(strId: self.dictData["id"].stringValue, indexChange: tag, status: status.uppercased())
            }
            else{
                self.doneTask(strId: task.tempTask!["task_id"].stringValue, indexChange: tag)
            }
            
        }
        // let action = UIAlertAction(title: mapping.string(forKey: "Ok_key"), style: .cancel, handler: nil)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func showOpenCamera(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            selectedIndex = sender.tag
            self.present(myPickerController, animated: true, completion: nil)
        }
    }


}

//MARK:- Tablview Delegate

extension TaskDetailsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let task = taskList[indexPath.row]
        let taskStatus = task.tempTask!["isTaskCompleted"].stringValue
        let taskTitle = task.tempTask!["title"].stringValue
        let taskImgRequire = task.tempTask!["picture_required"].stringValue
        let isImgUploaded = task.tempTask!["isImgUploaded"].stringValue
        
        if task.tempTask!["role"].stringValue == UserLogInType && indexPath.row  == 0
        {
            let TaskCell = self.tblTaskDetails.dequeueReusableCell(withIdentifier: "TaskDetailsPaidUnPaidStatusCell") as! TaskDetailsPaidUnPaidStatusCell
            TaskCell.isUserInteractionEnabled = false
            TaskCell.SetData(data: task.tempTask!)
            return TaskCell
        }
        
        if taskImgRequire == "true"
        {
            let TaskDetailsImgRequiereCell = self.tblTaskDetails.dequeueReusableCell(withIdentifier: "TaskDetailsImgRequiereCell") as! TaskDetailsImgRequiereCell
            TaskDetailsImgRequiereCell.lblTaskTitle.text  = taskTitle
            if isImgUploaded == "true" && taskStatus == "true"
            {
                TaskDetailsImgRequiereCell.isUserInteractionEnabled = false
                TaskDetailsImgRequiereCell.btnCheckUncheck.setImage(UIImage(named:"checkbox_selected"), for: .normal)
                TaskDetailsImgRequiereCell.imgTaskCheckUnCheck.image = UIImage(named:"ic_true_mark_green")
                let UploadedImg = taskList[indexPath.row]
                TaskDetailsImgRequiereCell.btnUploadImg.setImage(UploadedImg.taskImg!, for: .normal)
            }else{
                TaskDetailsImgRequiereCell.isUserInteractionEnabled = true
                TaskDetailsImgRequiereCell.btnCheckUncheck.tag = indexPath.row
                TaskDetailsImgRequiereCell.btnCheckUncheck.addTarget(self, action: #selector(showAlertWithConformation(sender:)), for: .touchUpInside)
                TaskDetailsImgRequiereCell.btnUploadImg.tag = indexPath.row
                TaskDetailsImgRequiereCell.btnUploadImg.addTarget(self, action: #selector(showAlertWithConformation(sender:)), for: .touchUpInside)
                TaskDetailsImgRequiereCell.btnCheckUncheck.setImage(UIImage(named:"checkbox_unselected"), for: .normal)
                TaskDetailsImgRequiereCell.imgTaskCheckUnCheck.image = UIImage(named: "ic_true_mark_green1")
            }
            return TaskDetailsImgRequiereCell
        }else{
            let TaskDetailsCell = self.tblTaskDetails.dequeueReusableCell(withIdentifier: "TaskDetailsCell") as! TaskDetailsCell
            TaskDetailsCell.lblTaskTitle.text = taskTitle
            if taskStatus == "true"
            {
                TaskDetailsCell.isUserInteractionEnabled = false
                TaskDetailsCell.btnCheckUncheck.setImage(UIImage(named:"checkbox_selected"), for: .normal)
                TaskDetailsCell.imgTaskCheckUnCheck.image = UIImage(named: "ic_true_mark_green")
            }else{
                TaskDetailsCell.isUserInteractionEnabled = true
                TaskDetailsCell.btnCheckUncheck.tag = indexPath.row
                TaskDetailsCell.btnCheckUncheck.addTarget(self, action: #selector(showAlertWithConformation(sender:)), for: .touchUpInside)
                
                TaskDetailsCell.btnCheckUncheck.setImage(UIImage(named:"checkbox_unselected"), for: .normal)
                TaskDetailsCell.imgTaskCheckUnCheck.image = UIImage(named: "ic_true_mark_green1")
            }
            return TaskDetailsCell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton()
        btn.tag = indexPath.row
        let task = taskList[indexPath.row]
        
        if task.tempTask!["isTaskCompleted"].boolValue == true
        {
            
        }
        else{
            
        }
        showAlertWithConformation(sender: btn)
        
    }
}
extension TaskDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            var task = self.taskList[selectedIndex]
            
            self.userProfileUpdate(strTaskId: task.tempTask!["task_id"].stringValue, imgUploaded: image, indexChange: selectedIndex)
            /*var task = self.taskList[selectedIndex]
            task.tempTask!["status"] = "true"
            task.tempTask!["isImgUploaded"] = "true"
            task.tempTask!["isTaskCompleted"] = "true"
            task.taskImg! = image
            self.taskList[selectedIndex] = task
            let indexpath = IndexPath(row: selectedIndex, section: 0)
            self.tblTaskDetails.reloadRows(at: [indexpath], with: .none)*/
        
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Service

extension TaskDetailsVC : NVActivityIndicatorViewable
{
    func doneTask(strId:String,indexChange:Int)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var url = String()
            url = "\(basicURL)\(strDoneTask)"
            
            let param =  ["id": strId]
            
            
            print("URL: \(url)")
            
            print("Param : \(param)")
            
            startAnimating(Loadersize, message: strLoader, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            CommonService().Service(url: url, param: param,header: getUserDetail("access_token"),isLogin: false) { (respones) in
                
               
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strFlagTrue
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                        var task = self.taskList[indexChange]
                     //   task.tempTask!["status"] = "true"
                        task.tempTask!["isTaskCompleted"] = "true"
                        self.taskList[indexChange].tempTask! = task.tempTask!
                        /*for (index) in 0...self.taskList.count-1
                        {
                            var tsk = self.taskList[index]
                            tsk.isTaskStarted! = true
                            self.taskList[index] = tsk
                        }*/
                        let indexpath = IndexPath(row: indexChange, section: 0)
                        self.tblTaskDetails.reloadRows(at: [indexpath], with: .none)
                        self.taskDone?.doneTaskCompleted()
                    }
                    else if json["flag"].stringValue == strFlagAccessDenid
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func userProfileUpdate(strTaskId:String,imgUploaded:UIImage,indexChange:Int)
    {
        var kUrl = String()
        kUrl = "\(basicURL)\(strDoneTask)"
        
        print("URL:- \(kUrl)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param =  ["id": strTaskId]
            
            print("parm \(param)")
            startAnimating(Loadersize, message:strLoader , type: NVActivityIndicatorType(rawValue:LoaderType))            //            show
            
            let PasswordString =  String(format: "\(basic_username):\(basic_password)")
            let PasswordData = PasswordString.data(using: .utf8)
            let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
            
            let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
            print("headers:==\(headers)")
            
            let unit64:UInt64 = 10_000_000
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in param
                {
                    print("\(key) \(value)")
                    multipartFormData.append((value ).data(using: .utf8)!, withName: key)
                }
                
                let imgData = UIImageJPEGRepresentation(imgUploaded, 0.7)
                multipartFormData.append(imgData!, withName: "photo", fileName:"image.jpeg", mimeType: "image/png")
                
                
            }, usingThreshold: unit64, to: kUrl, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
                print("encoding result:\(encodingResult)")
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                        //send progress using delegate
                    })
                    upload.responseSwiftyJSON(completionHandler: { (responce) in
                        //stop
                        self.stopAnimating()
                        //   print("response:==>\(responce)")
                        if let json = responce.result.value
                        {
                            print("json\(json)")
                            if json["flag"].stringValue == strFlagTrue
                            {
                                makeToastSnackbar(message: json["msg"].stringValue)
                                
                                var task = self.taskList[self.selectedIndex]
                              //  task.tempTask!["status"] = "true"
                                task.tempTask!["isImgUploaded"] = "true"
                                task.tempTask!["isTaskCompleted"] = "true"
                                task.taskImg! = imgUploaded
                                self.taskList[self.selectedIndex] = task
                                let indexpath = IndexPath(row: self.selectedIndex, section: 0)
                                self.tblTaskDetails.reloadRows(at: [indexpath], with: .none)
                                self.taskDone?.doneTaskCompleted()
                                
                              /*  var task = self.taskList[indexChange]
                                task.tempTask!["status"] = "true"
                                task.tempTask!["isTaskCompleted"] = "true"
                                self.taskList[indexChange].tempTask! = task.tempTask!
                                for (index) in 0...self.taskList.count-1
                                {
                                    var tsk = self.taskList[index]
                                    tsk.isTaskStarted! = true
                                    self.taskList[index] = tsk
                                }
                                let indexpath = IndexPath(row: indexChange, section: 0)
                                self.tblTaskDetails.reloadRows(at: [indexpath], with: .none)
                                self.taskDone?.doneTask()*/
                            }
                            else if json["flag"].stringValue == strFlagAccessDenid
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
                            }
                            else
                            {
                                makeToastSnackbar(message: json["msg"].stringValue)
                            }
                        }
                        else
                        {
                            makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                        }
                        
                    })
                    
                case .failure(let encodingError):
                    print(encodingError)
                    
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
            
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func startTask(strId:String,indexChange:Int,status:String)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var url = String()
            url = "\(basicURL)\(strIsWorking)"
            
            let param =  ["id": strId,
                          "status":status]
            
            
            print("URL: \(url)")
            
            print("Param : \(param)")
            
            startAnimating(Loadersize, message: strLoader, type: NVActivityIndicatorType(rawValue:LoaderType))
            
            CommonService().Service(url: url, param: param,header: getUserDetail("access_token"),isLogin: false) { (respones) in
                
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strFlagTrue
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                        var task = self.taskList[indexChange]
                        task.tempTask!["status"] = "true"
                        task.tempTask!["isTaskCompleted"] = "true"
                        self.taskList[indexChange].tempTask! = task.tempTask!
                        for (index) in 0...self.taskList.count-1
                         {
                         var tsk = self.taskList[index]
                         tsk.isTaskStarted! = true
                         self.taskList[index] = tsk
                         }
                        let indexpath = IndexPath(row: indexChange, section: 0)
                        self.tblTaskDetails.reloadRows(at: [indexpath], with: .none)
                        self.taskDone?.doneTaskCompleted()
                    }
                    else if json["flag"].stringValue == strFlagAccessDenid
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}

