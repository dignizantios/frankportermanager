//
//  GoodToKnowVC.swift
//  frankporter
//
//  Created by DK on 10/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

enum selectParentVC
{
    case goodToKnow
    case info
}

class GoodToKnowVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblThingsToremember: UILabel!
    @IBOutlet weak var txtViewThinks: UITextView!
    
     //MARK:- Delcaration
    
    var strEditId = String()
    var strSimpleString = String()
    var selectedVC = selectParentVC.goodToKnow
    var dictData = JSON()
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        decore()
        
        if selectedVC == .goodToKnow
        {
            getKnowThing()
        }
        
    }
    
    
    //MARK:- Custom Methods
    
    func decore()
    {
        //self.navigationController?.interactivePopGestureRecognizer?.delegate = self
     //   self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        //self.navigationController?.isNavigationBarHidden = false
        self.title = getCommonString(key: "Good_to_know_key")
        
        if selectedVC == .goodToKnow
        {
            let RightNavigationButton =  UIButton()
            RightNavigationButton.setImage(UIImage(named: "ic_edit_text_header"), for: .normal)
            RightNavigationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            RightNavigationButton.addTarget(self, action: #selector(EditThinkgs(sender:)), for: .touchUpInside)
            let customBarItem = UIBarButtonItem(customView: RightNavigationButton)
            self.navigationItem.rightBarButtonItem = customBarItem
        }
        else{
          
            txtViewThinks.text = dictData["note"].stringValue
            
        }
        
        self.navigationItem.hidesBackButton = true
        
        let backButton =  UIButton()
        backButton.setImage(UIImage(named: "ic_arrow_back_header"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.addTarget(self, action: #selector(backAction(sender:)), for: .touchUpInside)
        let customBarBackItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = customBarBackItem
        
        lblThingsToremember.font = themeFontWithReduseSize(size: 17, fontname: .light)
        lblThingsToremember.textColor = UIColor.appThemeBtnColor
        
        txtViewThinks.font = themeFontWithReduseSize(size: 16, fontname: .light)
        txtViewThinks.textColor = UIColor.appThemeLightFontColor
        txtViewThinks.contentOffset = CGPoint.zero
        txtViewThinks.layoutIfNeeded()
    }
    
    @objc func backAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func EditThinkgs(sender:UIButton)
    {
        let EditThinkVC = self.storyboard?.instantiateViewController(withIdentifier: "EditThinkVC") as! EditThinkVC
        EditThinkVC.userThink = txtViewThinks.text
        EditThinkVC.strId = strEditId
        EditThinkVC.editThink = self
        self.navigationController?.pushViewController(EditThinkVC, animated: true)
    }
    
}

//MARK:- Delegate

extension GoodToKnowVC:delegateEdit
{
    func editThink() {
        getKnowThing()
    }
}

//MARK:- Call API

extension GoodToKnowVC:NVActivityIndicatorViewable
{
    func getKnowThing()
    {
        var kURL = String()
        kURL = "\(basicURL)\(strGetGoodToKnow)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param:[String:String] =  [:]
            
            print("parma \(param)")
            
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            CommonService().Service(url: kURL, param: param,header: getUserDetail("access_token"),isLogin: false, completion: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        self.txtViewThinks.text  = json["data"]["good_to_know"].stringValue
                        self.strEditId = json["data"]["id"].stringValue
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else{
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}


