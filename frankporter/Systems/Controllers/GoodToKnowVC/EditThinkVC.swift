//
//  EditThinkVC.swift
//  frankporter
//
//  Created by DK on 10/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

protocol delegateEdit {
    func editThink()
    
}

class EditThinkVC: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var txtViewEditThink: UITextView!
    @IBOutlet weak var btnSave: xUIButton!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    //MARK:- Declrations
    var userThink = String()
    var strId = String()
    var editThink:delegateEdit?
    //MARK:- View lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = .init(rawValue: 0)
        self.extendedLayoutIncludesOpaqueBars = true
        
        decore()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    override func viewDidLayoutSubviews() {
        txtViewEditThink.setContentOffset(.zero, animated: false)
    }
    //MARK:- Custom Methods
    
    func decore()
    {
        //self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       // self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        //self.navigationController?.isNavigationBarHidden = false
        self.title = getCommonString(key: "Good_to_know_key")
        //self.navigationItem.hidesBackButton = true
        
        let backButton =  UIButton()
        backButton.setImage(UIImage(named: "ic_arrow_back_header"), for: .normal)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.addTarget(self, action: #selector(backAction(sender:)), for: .touchUpInside)
        let customBarBackItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = customBarBackItem
        
        if userThink == ""
        {
            self.lblPlaceholder.isHidden =  false
        }
        else{
            self.lblPlaceholder.isHidden =  true
        }
        
        txtViewEditThink.text = userThink
        txtViewEditThink.delegate = self
        txtViewEditThink.font = themeFontWithReduseSize(size: 17, fontname: .light)
        txtViewEditThink.textColor = UIColor.appThemeBtnColor
        txtViewEditThink.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        btnSave.titleLabel?.font = themeFontWithReduseSize(size: 20, fontname: .light)
        
        lblPlaceholder.text = getCommonString(key: "Enter_here_key")
        lblPlaceholder.font = themeFontWithReduseSize(size: 17, fontname: .light)
        lblPlaceholder.textColor = UIColor.appThemeBtnColor
        
    }
    
    //MARK:- Button Actions
    
    @objc func backAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        guard txtViewEditThink.text.trimmingCharacters(in: .whitespaces) != "" else {
            makeToastSnackbar(message:getValidationString(key: "Please_enter_your_think_key"))
            return
        }
        
        editKnowThing()
    }
    
}
extension EditThinkVC:UITextViewDelegate
{ 
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else{
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}

//MARK:- Call API

extension EditThinkVC:NVActivityIndicatorViewable
{
    func editKnowThing()
    {
        var kURL = String()
        kURL = "\(basicURL)\(strEditGoodToKnow)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param:[String:String] =  ["id":strId,
                                          "good_to_know":self.txtViewEditThink.text ?? ""]
            
            print("parma \(param)")
            
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            CommonService().Service(url: kURL, param: param,header: getUserDetail("access_token"),isLogin: false, completion: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        self.editThink?.editThink()
                        makeToastSnackbar(message: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else{
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}

