//
//  ReportIssueVC.swift
//  frankporter
//
//  Created by DK on 10/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

class ReportIssueVC: UIViewController {
    //MARK:- Outlets
    @IBOutlet weak var imgReportIssue: UIImageView!
    @IBOutlet weak var lbloSelecctCategory: UILabel!
    @IBOutlet weak var txtCategory: TextFieldDesinable!
    @IBOutlet weak var lblEnterDescription: UILabel!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var btnSend: xUIButton!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    //MARK:- Declaration
    var CategoryDD = DropDown()
    var Category = [String]()
    var arrCategory:[JSON] = []
    var selectionCatId = String()
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = .init(rawValue: 0)
        self.extendedLayoutIncludesOpaqueBars = true
        decore()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        
        self.configDD(dropdown: self.CategoryDD, sender: self.txtCategory)
        self.selectionSetting()
    }
    
    //MARK:- Custom Methods
    
    func decore()
    {
        //self.navigationController?.interactivePopGestureRecognizer?.delegate = self
       // self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        self.navigationController?.isNavigationBarHidden = false
        self.title = getCommonString(key: "Report_an_Issue_key")
        let BackNavigationButton =  UIButton()
        BackNavigationButton.setImage(UIImage(named: "ic_arrow_back_header"), for: .normal)
        BackNavigationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        BackNavigationButton.addTarget(self, action: #selector(backAction(sender:)), for: .touchUpInside)
        let customBarItem = UIBarButtonItem(customView: BackNavigationButton)
        self.navigationItem.leftBarButtonItem = customBarItem
        self.navigationItem.hidesBackButton = true
        
       // lbloSelecctCategory.textColor = UIColor.appThemeLightFontColor
      //  lbloSelecctCategory.font = themeFontWithReduseSize(size: 17, fontname: .light)
      //  lblEnterDescription.textColor = UIColor.appThemeLightFontColor
      //  lblEnterDescription.font = themeFontWithReduseSize(size: 17, fontname: .light)
        txtViewDescription.textColor = UIColor.appThemeLightFontColor
        txtViewDescription.font = themeFontWithReduseSize(size: 17, fontname: .light)
        txtViewDescription.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        btnSend.titleLabel?.font = themeFontWithReduseSize(size: 20, fontname: .light)
        
        [txtCategory].forEach { (txt) in
            txt?.delegate = self
            txt?.textColor = UIColor.appThemeBtnColor
            txt?.font = themeFontWithReduseSize(size: 17, fontname: .light)
        }
        /*if UserLogInType.lowercased() == "CheckIn".lowercased()
        {
            Category = ["Management","Maintenance","Clenner"]
        }else if UserLogInType.lowercased() == "Management".lowercased()
        {
            Category = ["Maintenance","Clenner","Check-In"]
            
        }else if UserLogInType.lowercased() == "Maintenance".lowercased()
        {
            Category = ["Management","Clenner","Check-In"]
        }else if UserLogInType.lowercased() == "Clenner".lowercased()
        {
            Category = ["Management","Maintenance","Check-In"]
        }else{
            Category = ["Management","Maintenance","Clenner","Check-In"]
        }*/
        
        getRole()
        
    }
    @objc func backAction(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - DropDown Setup
    
    func configDD(dropdown: DropDown, sender: UIView)
    {
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .manual
        //  dropdown.topOffset = CGPoint(x: 0, y: self.view.bounds.origin.y)
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height+5)
        // print("textfield Frame \(txtPurpose.frame)")
        dropdown.width = sender.bounds.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.appThemeBackgroundColor
        dropdown.textColor = UIColor.appThemeBtnColor
        dropdown.textFont = themeFontWithReduseSize(size: 18, fontname: .light)
        dropdown.selectionBackgroundColor = UIColor.clear
        self.CategoryDD.dataSource = Category
        self.view.layoutIfNeeded()
        
        
    }
    
    func selectionSetting()
    {
        self.CategoryDD.selectionAction = { (index, item) in
            self.txtCategory.text = item
            self.selectionCatId = self.arrCategory[index]["id"].stringValue
            self.view.endEditing(true)
        }
        
        
        
    }
    
    //MARK:- Button Action
    
    @IBAction func btnSendAction(_ sender: Any) {
        guard txtCategory.text?.trimmingCharacters(in: .whitespaces) != "" else {
            makeToastSnackbar(message: getValidationString(key: "Please_select_category_key"))
            return
        }
        
        guard txtViewDescription.text.trimmingCharacters(in: .whitespaces) != "" else {
            makeToastSnackbar(message:getValidationString(key: "Please_enter_description_key"))
            return
        }
        
        submitReport()
        
    }
    
    @IBAction func btnSelectCategory(_ sender: Any) {
        CategoryDD.show()
    }
    
    
}
extension ReportIssueVC:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  Category.count == 0
        {
            getRole()
        }
        else
        {
             CategoryDD.show()
        }
       
        return false
    }
}
extension ReportIssueVC:UITextViewDelegate
{
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else{
            self.lblPlaceholder.isHidden = true
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}

//MARK:- Call API

extension ReportIssueVC:NVActivityIndicatorViewable
{
    func submitReport()
    {
        var kURL = String()
        kURL = "\(basicURL)\(strReportIssue)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param =  ["category_id": selectionCatId,
                          "description":self.txtViewDescription.text ?? ""
                          ]
            
            print("parma \(param)")
            
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            CommonService().Service(url: kURL, param: param,header: getUserDetail("access_token"),isLogin: false, completion: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        self.navigationController?.popViewController(animated: true)
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else{
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
    func getRole()
    {
        var kURL = String()
        kURL = "\(basicURL)\(strRole)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param:[String:String] =  [:]
            
            print("parma \(param)")
            
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            CommonService().Service(url: kURL, param: param,header: getUserDetail("access_token"),isLogin: false, completion: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        self.Category = []
                        self.arrCategory = json["data"].arrayValue
                        for i in 0..<self.arrCategory.count{
                            self.Category.append(self.arrCategory[i]["role"].stringValue)
                        }
                        self.CategoryDD.dataSource = self.Category
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else{
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}

