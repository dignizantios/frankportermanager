//
//  TaskListCell.swift
//  frankporter
//
//  Created by DK on 07/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import SkeletonView
import SDWebImage

class TaskListCell: UITableViewCell {

    @IBOutlet weak var PropertyImg: xUIImageView!
    @IBOutlet weak var PropertyAddress: UILabel!
    @IBOutlet weak var TaskDate: UILabel!
    @IBOutlet weak var lblDivide: UILabel!
    @IBOutlet weak var taskStatusIcon: UIImageView!
    @IBOutlet weak var lblTaskType: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblFinishTimeInterval: UILabel!
    @IBOutlet weak var lblTaskCurrentStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        PropertyImg.layer.cornerRadius = 4
        PropertyImg.layer.masksToBounds = true
        PropertyImg.showAnimatedSkeleton()
        
        PropertyAddress.font = themeFontWithReduseSize(size: 18, fontname: .light)
        PropertyAddress.textColor = UIColor.appThemeBtnColor
        PropertyAddress.showAnimatedSkeleton()
        
        TaskDate.font = themeFontWithReduseSize(size: 18, fontname: .light)
        TaskDate.textColor = UIColor.appThemeLightFontColor
        TaskDate.showAnimatedSkeleton()
        
        lblTaskType.font = themeFontWithReduseSize(size: 18, fontname: .light)
        lblTaskType.showAnimatedSkeleton()
        
        lblTime.font = themeFontWithReduseSize(size: 14, fontname: .light)
        lblTime.showAnimatedSkeleton()        
        lblTime.textColor = UIColor.appThemeLightFontColor
        lblTime.text = "\(getCommonString(key: "Time_key")) : "
        
        lblFinishTimeInterval.font = themeFontWithReduseSize(size: 16, fontname: .light)
        lblFinishTimeInterval.showAnimatedSkeleton()
        lblFinishTimeInterval.textColor = UIColor.appThemeBtnColor
        
        lblTaskCurrentStatus.font = themeFontWithReduseSize(size: 14, fontname: .light)
        lblTaskCurrentStatus.showAnimatedSkeleton()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func stopAnimationSkeleton()
    {
        [lblTaskCurrentStatus,lblFinishTimeInterval,lblTime,TaskDate,PropertyAddress,lblTaskType].forEach { (lbl) in
            lbl?.hideSkeleton()
        }
        PropertyImg.hideSkeleton()
    }
    
    func setData(data:JSON)
    {
        PropertyImg.sd_setImage(with: data["image"].url, placeholderImage: UIImage(named: "image_placeholder_small"), options: .lowPriority, completed: nil)
        PropertyAddress.text = data["appartment"].stringValue
        
        let tmpDate = stringTodate(Formatter: "yyyy-MM-dd", strDate: data["date"].stringValue)
        let strCurrentDate = DateToString(Formatter: "yyyy-MM-dd", date: Date())
        let FinalDate = stringTodate(Formatter: "yyyy-MM-dd", strDate: strCurrentDate)
        
        if tmpDate == FinalDate
        {
            TaskDate.text = "Today"
        }
        else{
            let strFinalDate = DateToString(Formatter: "MM/dd/yyyy", date: tmpDate)
            TaskDate.text = strFinalDate
        }
        
        let userType = data["role"].stringValue
        let taskType = data["paid_status"].stringValue.lowercased()
        if userType != UserLogInType
        {
            lblDivide.isHidden = true
            taskStatusIcon.isHidden = true
            lblTaskType.isHidden = true
        }else{
            lblDivide.isHidden = false
            taskStatusIcon.isHidden = false
            lblTaskType.isHidden = false
            if taskType == "unpaid"
            {
                taskStatusIcon.image = UIImage(named: "ic_reservation_unpaid_small_listing")
                lblTaskType.text = taskType.capitalized
                lblTaskType.textColor = UIColor.appThemeRedColor
                
            }else{
                taskStatusIcon.image = UIImage(named: "ic_reservation_paid_small_listing")
                lblTaskType.text = taskType.capitalized
                lblTaskType.textColor = UIColor.appThemeGreenColor
            }
        }
        let taskStartTime = data["time_from"].stringValue
        let taskEndTime = data["time_to"].stringValue
        lblFinishTimeInterval.text = "\(taskStartTime) - \(taskEndTime)"
        
        let TaskStatus = data["task_status"].stringValue.lowercased()
        if TaskStatus == "todo"
        {
            lblTaskCurrentStatus.text = TaskStatus.uppercased()
            lblTaskCurrentStatus.textColor = UIColor.appThemeLightFontColor
        }else if TaskStatus == "working"{
            lblTaskCurrentStatus.text = TaskStatus.uppercased()
            lblTaskCurrentStatus.textColor = UIColor.appThemeYellowColor
        }else if TaskStatus == "stucked"{
            lblTaskCurrentStatus.text = TaskStatus.uppercased()
            lblTaskCurrentStatus.textColor = UIColor.appThemeRedColor
        }else{
            lblTaskCurrentStatus.text = TaskStatus.uppercased()
            lblTaskCurrentStatus.textColor = UIColor.appThemeGreenColor
        }
        
        
    }

}
