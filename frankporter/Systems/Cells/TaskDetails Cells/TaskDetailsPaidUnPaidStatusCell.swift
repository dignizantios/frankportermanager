//
//  TaskDetailsPaidUnPaidStatusCell.swift
//  frankporter
//
//  Created by DK on 07/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON

class TaskDetailsPaidUnPaidStatusCell: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblTaskTypeStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTaskTypeStatus.font = themeFontWithReduseSize(size: 16, fontname: .light)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func SetData(data:JSON)
    {
        let taskType = data["paid_status"].stringValue.lowercased()
        if taskType == "unpaid"
        {
            imgStatus.image = UIImage(named: "ic_reservation_unpaid")
            lblTaskTypeStatus.text = "This Reservation is Unpaid"
            lblTaskTypeStatus.textColor = UIColor.appThemeRedColor
        }else{
            imgStatus.image = UIImage(named: "ic_reservation_paid_small_listing")
            lblTaskTypeStatus.text = "This Reservation is Paid"
            lblTaskTypeStatus.textColor = UIColor.appThemeGreenColor
        }
        
    }

}
