//
//  TaskDetailsCell.swift
//  frankporter
//
//  Created by DK on 07/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class TaskDetailsCell: UITableViewCell {

    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet weak var imgTaskCheckUnCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTaskTitle.textColor = UIColor.appThemeBtnColor
        lblTaskTitle.font = themeFontWithReduseSize(size: 16, fontname: .light)
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
