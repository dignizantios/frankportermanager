//
//  User.swift
//  frankporter
//
//  Created by Jaydeep on 12/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation


class User:NSObject
{
    var strUsername : String = ""
    var strPassword : String = ""
    var strValidationMessage : String = ""
    
    
    //MARK:- Check Login Credential
    
    func isLogin() -> Bool {
        
        if strUsername.trimmingCharacters(in: .whitespaces).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_username_key")
            return false
        }
        else if strPassword == ""
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_password_key")
            return false
        }
        return true
    }
}
