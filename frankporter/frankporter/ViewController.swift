//
//  ViewController.swift
//  frankporter
//
//  Created by DK on 06/12/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit
import Crashlytics
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import NVActivityIndicatorView

class ViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var lblMnMtApp: UILabel!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    @IBOutlet weak var btnSignIn: xUIButton!
    @IBOutlet weak var constant_ViewBottom: NSLayoutConstraint!
    
    //MARK:- Declaration
    
    
    //MARK:- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if getUserDetail("access_token") != ""
        {
            let DashbaordVC = self.storyboard?.instantiateViewController(withIdentifier: "DashbaordVC") as! DashbaordVC
            self.navigationController?.pushViewController(DashbaordVC, animated: false)
        }
        decore()
      
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.txtUserName.text = ""
        self.txtPwd.text = ""
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true      
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        {
            let keyboardHeight = keyboardRectValue.height
            print("keyBorad Height : \(keyboardHeight) ")
            UIView.animate(withDuration: 0.5, animations: {
                self.constant_ViewBottom.constant = -keyboardHeight
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    @objc func keyboardWillHide(_ notification: NSNotification){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.constant_ViewBottom.constant = 0.0
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
    //MARK:- Custom Methods
    
    func decore()
    {
        [txtUserName,txtPwd].forEach { (txt) in
            txt?.delegate = self
            txt?.textColor = UIColor.appThemeBtnColor
            txt?.font = themeFont(size: 16.0, fontname: themeFonts.book)
        }
        self.navigationController?.isNavigationBarHidden = true
        lblSignIn.font = themeFontWithReduseSize(size: 20.0, fontname: themeFonts.light)
        lblMnMtApp.font = themeFontWithReduseSize(size: 18.0, fontname: themeFonts.light)
        lblSignIn.textColor = UIColor.appThemeBtnColor
        lblMnMtApp.textColor = UIColor.appThemeBtnColor
        btnSignIn.backgroundColor = UIColor.appThemeBtnColor
        btnSignIn.titleLabel?.font = themeFontWithReduseSize(size: 18.0, fontname: themeFonts.light)
        
    }
    
     //MARK:- Button Actions
    
    @IBAction func btnSignInAction(_ sender: UIButton)
    {
        objUser.strUsername = self.txtUserName.text ?? ""
        objUser.strPassword = self.txtPwd.text ?? ""
        
        if objUser.isLogin()
        {
            loginUser()
        }
        else
        {
            makeToastSnackbar(message: objUser.strValidationMessage)
        }
    }
    

}
extension ViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


//MARK:- Call API

extension ViewController:NVActivityIndicatorViewable
{
    func loginUser()
    {
        var kURL = String()
        kURL = "\(basicURL)\(strLogin)"
        
        print("URL:- \(kURL)")
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {            
            
            let param =  ["username": self.txtUserName.text ?? "",
                          "password": self.txtPwd.text ?? ""
                          ]
            
            print("parma \(param)")
            
            startAnimating(Loadersize, type: NVActivityIndicatorType(rawValue: LoaderType))
            CommonService().Service(url: kURL, param: param,header: "",isLogin: true, completion: { (respones) in
                
                self.stopAnimating()
                
                if let json = respones.value
                {
                    print("json \(json)")
                    if json["flag"].stringValue == strFlagTrue
                    {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let DashbaordVC = self.storyboard?.instantiateViewController(withIdentifier: "DashbaordVC") as! DashbaordVC
                        self.navigationController?.pushViewController(DashbaordVC, animated: true)
                    }
                    else
                    {
                        makeToastSnackbar(message: json["msg"].stringValue)
                    }
                }
                else{
                    makeToastSnackbar(message: getCommonString(key: "Something_went_wrong_key"))
                }
            })
        }
        else
        {
            makeToastSnackbar(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}
